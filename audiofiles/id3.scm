(define-module (audiofiles id3)
  #:use-module (rnrs io ports)
  #:use-module (rnrs bytevectors)
  #:use-module (rnrs arithmetic bitwise)
  #:use-module (rnrs records syntactic)
  #:use-module (ice-9 match)
  #:use-module (ice-9 iconv)
  #:use-module (ice-9 optargs)
  #:use-module (ice-9 receive))


(define-record-type id3-tag
  (fields (mutable filename)
          (mutable version)
          (mutable flags)
          (mutable size)
          (mutable frames)))

(define-record-type id3-frame 
  (fields (mutable id)))


(define (integer->syncsafe int)
  (bitwise-ior (bitwise-and (bitwise-arithmetic-shift int 3) #x7f000000)
               (bitwise-and (bitwise-arithmetic-shift int 2) #x7f0000)
               (bitwise-and (bitwise-arithmetic-shift int 1) #x7f00)
               (bitwise-and #x7f)))

(define (syncsafe->integer bv)
  (bitwise-ior (bitwise-arithmetic-shift (bytevector-u8-ref bv 0) 21)
               (bitwise-arithmetic-shift (bytevector-u8-ref bv 1) 14)
               (bitwise-arithmetic-shift (bytevector-u8-ref bv 2) 17)
               (bytevector-u8-ref bv 3)))


(define (read-header port)
  (define identifier
    (bytevector->string (get-bytevector-n port 3) "ISO-8859-1"))
  (define version
    (cons 2 (bytevector->u8-list (get-bytevector-n port 2))))
  (define flags
    (bytevector-u8-ref (get-bytevector-n port 1) 0))

  (define size (syncsafe->integer (get-bytevector-n port 4)))

  (values version flags size))

(define (padding? port)
  (= (lookahead-u8 port) 0))

(define (read-text-frame bv version)
  "stub")

(define (read-frame-header port version)

  (define frame-id (bytevector->string (get-bytevector-n port 4) "ISO-8859-1"))
  (define size (bytevector-u32-ref (get-bytevector-n port 4) 0 (endianness big)))
  (define flags (get-bytevector-n port 2))

  (values frame-id size flags))

(define (read-frame port version)
  (receive (frame-id frame-size frame-flags)
      (read-frame-header port version)
    (let ((payload (get-bytevector-n port frame-size)))
      (values
       frame-size
       `(,frame-id (i-will-be-a-frame-record))))))

(define* (read-frames port size version)
  (call/cc
   (lambda (end-of-frames)
     (let frame-loop ((size size)
                      (frames '()))
       (receive (bytes-read frame)
           (if (padding? port)
               (end-of-frames frames)
               (read-frame port version))
         (let ((bytes-left (- size bytes-read)))
           (if (< 0 bytes-left)
               (frame-loop bytes-left
                           (cons frame frames))
               (cons frame frames))))))))


(define (id3-tag filename)
  (call-with-input-file filename
    (lambda (port)
      (call-with-values
          (lambda () (read-header port))
        (lambda (version flags size)
          (make-id3-tag filename version flags size (read-frames port size version)))))
    #:binary #t))
